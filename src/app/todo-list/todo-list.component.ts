import { Component, OnInit, HostBinding } from '@angular/core';
import { TodoItem } from '../models/todo-item.models';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  @HostBinding('attr.class') cssClass = 'container content mt-4';
  todoList: TodoItem[] = [];

  constructor() { 
    this.todoList.push(new TodoItem("Regar las plantas", new Date()));
    this.todoList.push(new TodoItem("Ir a recoger el paquete a casa de la abuela", new Date()));
    this.todoList.push(new TodoItem("Comprar manzanas en el supermercado", new Date()));
    this.todoList.push(new TodoItem("Comprar el regalo para el cumpleaños de Roger", new Date()));
  }

  ngOnInit() {
  }

  addItem(name: string): boolean {
    if(name != "")
      this.todoList.push(new TodoItem(name, new Date()));
    return false;
  }

}
